using Microsoft.VisualBasic.Logging;
using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace 控件使用实例
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            //绑定数据
            List<BindData.tb_user> dataSource = BindData.tb_user.InitData();
            this.cb_usr_list.DataSource = dataSource;//这里查到的是实体的所有数据，会有什么不好的地方？
            //var res = dataSource as IEnumerable<BindData.tb_user>;
            //var data = res.SelectMany(u=>u.tb_User_LastLogin,(y,u)=>new {u.ID,u.Email,y });
            //this.cb_usr_list.DataSource =
            this.cb_usr_list.ValueMember = "ID";
            this.cb_usr_list.DisplayMember = "Name";

            List<Person> personList = new List<Person>
{
    new Person
    {
        Name = "P1", Age = 18, Gender = "Male",
        Dogs = new Dog[]
        {
            new Dog { Name = "D1" },
            new Dog { Name = "D2" }
        }
    },
    new Person
    {
        Name = "P2", Age = 19, Gender = "Male",
        Dogs = new Dog[]
        {
            new Dog { Name = "D3" }
        }
    },
    new Person
    {
        Name = "P3", Age = 17,Gender = "Female",
        Dogs = new Dog[]
        {
            new Dog { Name = "D4" },
            new Dog { Name = "D5" },
            new Dog { Name = "D6" }
        }
    }
};

            var results = from x in personList select new { x.Age,x.Name,x.Gender};
            var res1 = personList.Select(x=>x.Name);
            var res2 = personList.SelectMany(x => x.Name);
            var res3 = personList.SelectMany(x=>x.Dogs);

            
        }
    


    }
    /*听课之前先关注，学习不迷路*/
    /*主要讲解.net控件的使用及linq中的select与selectmany*/

    /*模拟数据
     */


    class Person
    {
        public string Name { set; get; }
        public int Age { set; get; }
        public string Gender { set; get; }
        public Dog[] Dogs { set; get; }
    }
    public class Dog
    {
        public string Name { set; get; }
    }


}

namespace BindData
{
    public class tb_user_lastLogin
    {
        public int ID { get; set; }
        public int UserId { get; set; }
        public DateTime LastLoginTime { get; set; }
    }

    public class tb_user
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public tb_user_lastLogin tb_User_LastLogin { get; set; }



        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <returns></returns>
        public static List<tb_user> InitData()
        {
            List<tb_user> users = new List<tb_user>();

            for (int i = 0; i < 100; i++)
            {
                users.Add(new tb_user() { ID = i, Name = $"name-{i}", Email = $"email_{i}@qq.com" });
            }
            return users;

        }
    }
}