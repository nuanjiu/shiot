﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArticleCollection
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            bool createNew;
            Mutex mt = new Mutex(true, "ApplicationMutex", out createNew);
            if (createNew)
            {

               // Application.Run(new MainWindow());
            }
            else
            {
                MessageBox.Show("程序已经打开！");
                Process.GetCurrentProcess().Kill();
            }
        }





        /*吴恒阳的女朋友最近要和他分手，但是他女朋友说给他最后一次机会，
         * 就是去他家一次，看看他的厨房能力，如果能做得一手好菜，并且也快，那么就考虑和好
         * 小吴同学这样想：我一定要准备很多菜，很多汤
                           我一定要快速做完
           这时，可以理解为厨房就是进程，用来做菜做饭的，锅就是进程，用来做菜，所以我们要么一个锅慢慢做，要么多个锅一起做。
           这时我们的酱油、调味、盐就是资源，各个锅（进程）之间就会抢酱油、盐（资源），并且我们还是根据各个菜的情况来判断我们
           要做的工作（如什么时候加盐，什么时候加酱油），这就是线程的特性（调度、抢占式执行）
           这里可以想一下：我们要做一个或是拆一个厨房比加一个锅小一个锅肯定前者代价大，所以我们进程的开销永远大于线程，并且一个
        进程中可以包括多个线程，这里可以理解为进程是资源调度单位，线程是最小执行单位。
        多线程共享进程资源（IO，cpu），多个进程之间资源独立，多个线程共享进程资源
        */

        /// <summary>
        /// 单线程效果（不符合女朋友的期望，太慢了）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Thread.Sleep(5000);
            MessageBox.Show("开始做第一个菜");
            Thread.Sleep(5000);
            MessageBox.Show("开始做第一个汤");
        }
        /// <summary>
        /// 线程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_Threed(object sender, RoutedEventArgs e)
        {
            Thread t = new Thread(() =>
            {

                Thread.Sleep(5000);
                MessageBox.Show("开始做第一个菜");
                Thread.Sleep(5000);
                MessageBox.Show("开始做第一个汤");
            });
            t.Start();
        }
        /// <summary>
        ///基于Task,Task是对线程的高级封装，底层使用的是还是Threed,所有的封装都是为了方便，高效
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_Task(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
            {

                Thread.Sleep(5000);
                MessageBox.Show("开始做第一个菜");
                Thread.Sleep(5000);
                MessageBox.Show("开始做第一个汤");
            });
        }

        private void Button_Click_Parallel(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
            {

                Thread.Sleep(4000);
                MessageBox.Show("开始做第一个菜");

            });

            Task.Run(() =>
            {


                Thread.Sleep(5000);
                MessageBox.Show("开始做第一个汤");
            });
        }

        private void Button_Click_Finish_False(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
            {

                Thread.Sleep(4000);
                MessageBox.Show("开始做第一个菜");

            });

            Task.Run(() =>
            {


                Thread.Sleep(5000);
                MessageBox.Show("开始做第一个汤");
            });
            MessageBox.Show("女朋友开始吃饭了");
        }

        private async void Button_Click_Finish_False_1(object sender, RoutedEventArgs e)
        {
            await Task.Run(() =>
            {

                Thread.Sleep(4000);
                MessageBox.Show("开始做第一个菜");

            });

            await Task.Run(() =>
            {


                Thread.Sleep(5000);
                MessageBox.Show("开始做第一个汤");
            });
            MessageBox.Show("女朋友开始吃饭了");
        }

        private void Button_Click_Finish(object sender, RoutedEventArgs e)
        {
            List<Task> tasks = new List<Task>();
            tasks.Add(Task.Run(() =>
            {
                Thread.Sleep(4000);
                MessageBox.Show("开始做第一个菜");

            }));
            tasks.Add(Task.Run(() =>
            {
                Thread.Sleep(5000);
                MessageBox.Show("开始做第一个汤");

            }));
            Task.WhenAll(tasks).ContinueWith(t =>
            {
                MessageBox.Show("女朋友开始吃饭了");

            });
        }
    }
}
