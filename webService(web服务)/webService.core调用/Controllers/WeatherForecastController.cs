using Microsoft.AspNetCore.Mvc;

namespace webService.core调用.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get()
        {

            //DemoServiceReference.DemoWebServiceSoapClient demoWebServiceSoapClient = new DemoServiceReference.DemoWebServiceSoapClient(DemoServiceReference.DemoWebServiceSoapClient.EndpointConfiguration.DemoWebServiceSoap);
            //string res = demoWebServiceSoapClient.HelloWorld();

            DemoServiceReference.DemoWebServiceSoapClient demoWebServiceSoapClient = new DemoServiceReference.DemoWebServiceSoapClient(DemoServiceReference.DemoWebServiceSoapClient.EndpointConfiguration.DemoWebServiceSoap);
           string res= demoWebServiceSoapClient.GetUserName("李四");
            return res;
        }
    }
}