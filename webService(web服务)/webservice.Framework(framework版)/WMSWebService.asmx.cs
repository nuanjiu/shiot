﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Services;

namespace webservice.Framework
{
    /// <summary>
    /// WMSWebService 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    // [System.Web.Script.Services.ScriptService]
    public class WMSWebService : System.Web.Services.WebService
    {
        /// <summary>
        /// 根据订单编号获取订单对应的物料数据
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [WebMethod]
        public List<Product> GetDataByOrderId(string orderId)
        {
           return Product.InitData().Where(x=>x.orderId==orderId).ToList();
        }
    }

   

    public class Product
    {
        public string ProductName { get; set; }
        public int ProductNum { get; set; }
        public string orderId { get; set; }

        public static List<Product> InitData()
        {
            List<Product> products= new List<Product>();
              products.Add(new Product() {  ProductName="电池", ProductNum=10000,orderId="1000"});
            products.Add(new Product() { ProductName = "发电机", ProductNum = 10000, orderId = "1000" });
            products.Add(new Product() { ProductName = "显示板", ProductNum = 10000, orderId = "1001" });
            return products;
        }
    }
}
