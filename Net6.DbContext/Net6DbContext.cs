﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Net6.DbContext.Models;

namespace Net6.DbContext
{
    public class Net6DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer("Data Source=.;Initial Catalog=cap_demo;Integrated Security=True");
        }

        public DbSet<tb_user_view> tb_user_view { get; set; }

        public DbSet<tb_staff> tb_Staff { get; set; }

        public DbSet<tb_teacher> tb_Teachers { get; set; }  

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<tb_user_view>(buid => { 
            buid.HasBaseType((Type)null);
                buid.ToView(null);
                buid.HasNoKey();
            });
        }
    }

    public class tb_user_view
    {
        public string DepartName { get; set; }
        public int DepartId { get; set; }
        public string StaffName { get; set; }
    }
}