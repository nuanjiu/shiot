﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net6.DbContext.Models
{
    /// <summary>
    /// 老师实体
    /// </summary>
    public class tb_teacher
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(10)]
        public string TeachName { get; set; }
        public string TeachIdCard { get; set; }
        public string TeachPassword { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? UpdateTime { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
