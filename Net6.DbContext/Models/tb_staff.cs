﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net6.DbContext.Models
{
    public class tb_staff
    {

        public int Id { get; set; }
        public string DepartName { get; set; }

        public int DepartId { get; set; }

        public string StaffName { get; set; }

        public bool Online { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public DateTime? OffLineTime { get; set; }


    }
}
