using Microsoft.AspNetCore.Mvc;

namespace rabbitmq.net.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        RabbitMQHelper helper = new RabbitMQHelper();
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public void PublishMsg()
        {
            //业务逻辑自己可以模拟
        
            helper.PublishMsg("wms", "测试信息");
        }

        [HttpGet("recive")]
        public void ReciveMsg()
        {
  
            helper.ReciveMsg("wms",(msg)=>{
                var res = msg;
            

            });
        }
    }
}