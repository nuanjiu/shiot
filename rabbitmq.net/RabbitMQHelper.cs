﻿namespace rabbitmq.net
{
    using RabbitMQ.Client;
    using RabbitMQ.Client.Events;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;

    public class RabbitMQHelper
    {
        ConnectionFactory? connectionFactory;
        IConnection? connection;
        IModel channel;
        string? exchange;

        public RabbitMQHelper(string _exchange = "fanout_mq")
        {
            exchange = _exchange;
            connectionFactory = new ConnectionFactory()
            {
                HostName = "localhost",
                Password = "guest",
                UserName = "guest"

            };
            connection = connectionFactory.CreateConnection();
            channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange, ExchangeType.Topic);
           
        }

        public void PublishMsg<T>(string queue, T msg) where T : class
        {
            //声明一个队列
            channel.QueueDeclare(queue, true, false, false, null);
            //绑定队列，交换机，路由
            channel.QueueBind(queue, exchange, queue);
            var basicProperties = channel.CreateBasicProperties();
            basicProperties.DeliveryMode = 2;
            //var payload = Encoding.UTF8.GetBytes("sff");
            MemoryStream memoryStream = new MemoryStream();
            //            BinaryFormatter binaryFormatter = new BinaryFormatter();
            //#pragma warning disable SYSLIB0011
            //            binaryFormatter.Serialize(memoryStream, msg);

            //            BinaryReader reader = new BinaryReader(memoryStream);
            //            var payload = memoryStream.GetBuffer();
            //  var payload = Encoding.UTF8.GetBytes("sff");
            //BinaryFormatter这个是有安全问题的，因为在.net生态中有很多使用。开放性很高，所有不让使用。采用下面的方法。参考地址：
            //https://learn.microsoft.com/zh-cn/dotnet/standard/serialization/binaryformatter-security-guide
            //https://learn.microsoft.com/zh-cn/dotnet/api/system.runtime.serialization.datacontractserializer?view=net-7.0


            DataContractSerializer dataContractSerializer = new DataContractSerializer(typeof(T));

            dataContractSerializer.WriteObject(memoryStream, msg);
            var payload= memoryStream.ToArray();
           

            var address = new PublicationAddress(ExchangeType.Direct, exchange, queue);
            channel.BasicPublish(address, basicProperties, payload);
        }

        public void ReciveMsg(string queue,Action<string> recived)
        {
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            consumer.Received += (ch, main) =>
            {
                string mes = Encoding.UTF8.GetString(main.Body.ToArray());
                recived(mes);
               
                channel.BasicAck(main.DeliveryTag, false);

            };
            //noAck = true，不需要回复，接收到消息后，queue上的消息就会清除
            //noAck = false，需要回复，接收到消息后，queue上的消息不会被清除，直到调用channel.basicAck(deliveryTag, false); queue上的消息才会被清除 而且，在当前连接断开以前，其它客户端将不能收到此queue上的消息
            channel.BasicConsume(queue, false, consumer);
           
        }
    }
}
