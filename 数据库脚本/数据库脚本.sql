USE [cap_demo]
GO
/****** Object:  Schema [cap]    Script Date: 2023/10/9 11:06:29 ******/
CREATE SCHEMA [cap]
GO
/****** Object:  Table [cap].[Published]    Script Date: 2023/10/9 11:06:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cap].[Published](
	[Id] [bigint] NOT NULL,
	[Version] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Content] [nvarchar](max) NULL,
	[Retries] [int] NOT NULL,
	[Added] [datetime2](7) NOT NULL,
	[ExpiresAt] [datetime2](7) NULL,
	[StatusName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_cap.Published] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [cap].[Received]    Script Date: 2023/10/9 11:06:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cap].[Received](
	[Id] [bigint] NOT NULL,
	[Version] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Group] [nvarchar](200) NULL,
	[Content] [nvarchar](max) NULL,
	[Retries] [int] NOT NULL,
	[Added] [datetime2](7) NOT NULL,
	[ExpiresAt] [datetime2](7) NULL,
	[StatusName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_cap.Received] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_staff]    Script Date: 2023/10/9 11:06:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_staff](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[DepartName] [nchar](10) NULL,
	[DepartId] [int] NULL,
	[StaffName] [nchar](10) NULL,
	[Online] [bit] NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[offLineTime] [datetime] NULL,
 CONSTRAINT [PK_tb_staff] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [cap].[Published] ([Id], [Version], [Name], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621961986393042945, N'v1', N'Order.Created', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621961986393042945","cap-corr-id":"6621961986393042945","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/7 20:03:55"},"Value":"hello\uFF0C\u8BA2\u5355\u521B\u5EFA\u6210\u529F\u5566"}', 0, CAST(N'2023-10-07T20:03:55.8866667' AS DateTime2), CAST(N'2023-10-08T20:03:57.8933333' AS DateTime2), N'Succeeded')
INSERT [cap].[Published] ([Id], [Version], [Name], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621961986393042947, N'v1', N'Order.Created', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621961986393042947","cap-corr-id":"6621961986393042947","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/7 20:06:57"},"Value":"hello\uFF0C\u8BA2\u5355\u521B\u5EFA\u6210\u529F\u5566"}', 0, CAST(N'2023-10-07T20:06:57.3666667' AS DateTime2), CAST(N'2023-10-08T20:06:57.4200000' AS DateTime2), N'Succeeded')
INSERT [cap].[Published] ([Id], [Version], [Name], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621961987256528897, N'v1', N'Order.Created', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621961987256528897","cap-corr-id":"6621961987256528897","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/7 20:07:26"},"Value":"hello\uFF0C\u8BA2\u5355\u521B\u5EFA\u6210\u529F\u5566"}', 0, CAST(N'2023-10-07T20:07:26.6733333' AS DateTime2), CAST(N'2023-10-08T20:07:28.1033333' AS DateTime2), N'Succeeded')
INSERT [cap].[Published] ([Id], [Version], [Name], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621961987378237441, N'v1', N'Order.Created', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621961987378237441","cap-corr-id":"6621961987378237441","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/7 20:07:57"},"Value":"hello\uFF0C\u8BA2\u5355\u521B\u5EFA\u6210\u529F\u5566111"}', 0, CAST(N'2023-10-07T20:07:57.0433333' AS DateTime2), CAST(N'2023-10-08T20:07:58.5200000' AS DateTime2), N'Succeeded')
INSERT [cap].[Published] ([Id], [Version], [Name], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621962192909910017, N'v1', N'Order.Created', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621962192909910017","cap-corr-id":"6621962192909910017","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/8 10:05:47"},"Value":"\u8BA2\u5355\u521B\u5EFA\u6210\u529F"}', 0, CAST(N'2023-10-08T10:05:47.6333333' AS DateTime2), CAST(N'2023-10-09T10:05:50.5066667' AS DateTime2), N'Succeeded')
INSERT [cap].[Published] ([Id], [Version], [Name], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621962194549862401, N'v1', N'Order.Created', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621962194549862401","cap-corr-id":"6621962194549862401","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/8 10:10:57"},"Value":"\u8981\u7ED9\u5230\u8BA2\u9605\u8005\u7684\u53C2\u6570"}', 0, CAST(N'2023-10-08T10:10:57.1533333' AS DateTime2), CAST(N'2023-10-09T10:10:57.3200000' AS DateTime2), N'Succeeded')
INSERT [cap].[Published] ([Id], [Version], [Name], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621962258996092929, N'v1', N'Order.Created', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621962258996092929","cap-corr-id":"6621962258996092929","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"Product[]","cap-senttime":"2023/10/8 14:33:36"},"Value":[{"ProductName":"\u7535\u6C60","ProductNum":10000,"orderId":"1000"},{"ProductName":"\u53D1\u7535\u673A","ProductNum":10000,"orderId":"1000"}]}', 0, CAST(N'2023-10-08T14:33:36.8500000' AS DateTime2), CAST(N'2023-10-09T14:33:40.0033333' AS DateTime2), N'Succeeded')
GO
INSERT [cap].[Received] ([Id], [Version], [Name], [Group], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621961986393042946, N'v1', N'Order.Created', N'cap.queue.demo.cap.v1', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621961986393042945","cap-corr-id":"6621961986393042945","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/7 20:03:55","cap-msg-group":"cap.queue.demo.cap.v1","cap-exec-instance-id":"DESKTOP-KB6JRPJ"},"Value":"hello\uFF0C\u8BA2\u5355\u521B\u5EFA\u6210\u529F\u5566"}', 0, CAST(N'2023-10-07T20:03:57.9966667' AS DateTime2), CAST(N'2023-10-08T20:04:03.0700000' AS DateTime2), N'Succeeded')
INSERT [cap].[Received] ([Id], [Version], [Name], [Group], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621961986393042948, N'v1', N'Order.Created', N'cap.queue.demo.cap.v1', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621961986393042947","cap-corr-id":"6621961986393042947","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/7 20:06:57","cap-msg-group":"cap.queue.demo.cap.v1","cap-exec-instance-id":"DESKTOP-KB6JRPJ"},"Value":"hello\uFF0C\u8BA2\u5355\u521B\u5EFA\u6210\u529F\u5566"}', 0, CAST(N'2023-10-07T20:07:01.8966667' AS DateTime2), CAST(N'2023-10-08T20:07:02.9066667' AS DateTime2), N'Succeeded')
INSERT [cap].[Received] ([Id], [Version], [Name], [Group], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621961987256528898, N'v1', N'Order.Created', N'cap.queue.demo.cap.v1', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621961987256528897","cap-corr-id":"6621961987256528897","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/7 20:07:26","cap-msg-group":"cap.queue.demo.cap.v1","cap-exec-instance-id":"DESKTOP-KB6JRPJ"},"Value":"hello\uFF0C\u8BA2\u5355\u521B\u5EFA\u6210\u529F\u5566"}', 0, CAST(N'2023-10-07T20:07:28.1600000' AS DateTime2), CAST(N'2023-10-08T20:07:28.3866667' AS DateTime2), N'Succeeded')
INSERT [cap].[Received] ([Id], [Version], [Name], [Group], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621961987378237442, N'v1', N'Order.Created', N'cap.queue.demo.cap.v1', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621961987378237441","cap-corr-id":"6621961987378237441","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/7 20:07:57","cap-msg-group":"cap.queue.demo.cap.v1","cap-exec-instance-id":"DESKTOP-KB6JRPJ"},"Value":"hello\uFF0C\u8BA2\u5355\u521B\u5EFA\u6210\u529F\u5566111"}', 0, CAST(N'2023-10-07T20:07:58.6366667' AS DateTime2), CAST(N'2023-10-08T20:08:07.6266667' AS DateTime2), N'Succeeded')
INSERT [cap].[Received] ([Id], [Version], [Name], [Group], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621962192909910018, N'v1', N'Order.Created', N'cap.queue.demo.cap.v1', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621962192909910017","cap-corr-id":"6621962192909910017","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/8 10:05:47","cap-msg-group":"cap.queue.demo.cap.v1","cap-exec-instance-id":"DESKTOP-KB6JRPJ"},"Value":"\u8BA2\u5355\u521B\u5EFA\u6210\u529F"}', 0, CAST(N'2023-10-08T10:05:50.7133333' AS DateTime2), CAST(N'2023-10-09T10:07:12.7733333' AS DateTime2), N'Succeeded')
INSERT [cap].[Received] ([Id], [Version], [Name], [Group], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621962194549862402, N'v1', N'Order.Created', N'cap.queue.demo.cap.v1', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621962194549862401","cap-corr-id":"6621962194549862401","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"String","cap-senttime":"2023/10/8 10:10:57","cap-msg-group":"cap.queue.demo.cap.v1","cap-exec-instance-id":"DESKTOP-KB6JRPJ"},"Value":"\u8981\u7ED9\u5230\u8BA2\u9605\u8005\u7684\u53C2\u6570"}', 0, CAST(N'2023-10-08T10:10:57.4900000' AS DateTime2), CAST(N'2023-10-09T10:10:57.7033333' AS DateTime2), N'Succeeded')
INSERT [cap].[Received] ([Id], [Version], [Name], [Group], [Content], [Retries], [Added], [ExpiresAt], [StatusName]) VALUES (6621962258996092930, N'v1', N'Order.Created', N'cap.queue.demo.cap.v1', N'{"Headers":{"cap-callback-name":null,"cap-msg-id":"6621962258996092929","cap-corr-id":"6621962258996092929","cap-corr-seq":"0","cap-msg-name":"Order.Created","cap-msg-type":"Product[]","cap-senttime":"2023/10/8 14:33:36","cap-msg-group":"cap.queue.demo.cap.v1"},"Value":[{"ProductName":"\u7535\u6C60","ProductNum":10000,"orderId":"1000"},{"ProductName":"\u53D1\u7535\u673A","ProductNum":10000,"orderId":"1000"}]}', 0, CAST(N'2023-10-08T14:33:40.1900000' AS DateTime2), NULL, N'Scheduled')
GO
SET IDENTITY_INSERT [dbo].[tb_staff] ON 

INSERT [dbo].[tb_staff] ([id], [DepartName], [DepartId], [StaffName], [Online], [CreateTime], [UpdateTime], [offLineTime]) VALUES (1, N'技术部       ', 1000, N'张三        ', 1, CAST(N'2023-10-08T14:53:08.043' AS DateTime), CAST(N'2023-10-09T14:53:38.207' AS DateTime), NULL)
INSERT [dbo].[tb_staff] ([id], [DepartName], [DepartId], [StaffName], [Online], [CreateTime], [UpdateTime], [offLineTime]) VALUES (2, N'财务部       ', 2000, N'李四        ', 0, CAST(N'2023-10-08T14:53:08.043' AS DateTime), CAST(N'2023-10-09T14:53:38.207' AS DateTime), CAST(N'2023-10-09T14:54:07.780' AS DateTime))
INSERT [dbo].[tb_staff] ([id], [DepartName], [DepartId], [StaffName], [Online], [CreateTime], [UpdateTime], [offLineTime]) VALUES (3, N'人事部       ', 3000, N'王五        ', 1, CAST(N'2023-10-08T14:53:08.043' AS DateTime), CAST(N'2023-10-09T14:53:38.207' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tb_staff] OFF
GO
