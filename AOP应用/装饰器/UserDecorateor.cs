﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AOP应用.装饰器
{
    public class UserDecorateor
    {
        HttpContext? _httpContext;
        public void UserService(HttpContext httpContext)
        {
            _httpContext = httpContext;
        }
        public static void Main()
        {
            //基础调用
            IUserService userService=new UserService();
            userService.UserLogin("admin", "123456");
           
            userService =new UserServiceDecorator(userService,_httpContext);
            userService.UserLogin("admin", "123456");

        }
    }
}
