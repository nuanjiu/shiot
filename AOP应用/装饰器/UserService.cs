﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOP应用.装饰器
{
    public class UserService : IUserService
    {
        public string UserLogin(string userName, string userPassword)
        {
            if (userName == "admin" && userPassword == "123456")
            {
                return "登录成功";
            }
            else
            {
                return "登录失败";
            }
        }
    }
}
