﻿using dotnet123.Model;
using Microsoft.EntityFrameworkCore;

namespace Dotnet123.EFCore
{
    public class BusinessDbContext : DbContext
    {

        public BusinessDbContext(DbContextOptions<BusinessDbContext> options) : base(options)
        {

        }

        public DbSet<tb_User>? tb_Users { get; set; }
    }
}