﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataSource.Lib.Models
{
    public class Staff
    {
        public int Id { get; set; }
        public string DepartName { get; set; }
        public int DepartId { get; set; }
        public string StaffName { get; }

        /// <summary>
        /// 是否离职
        /// </summary>
        public bool Online { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 离职时间
        /// </summary>
        public DateTime offLineTime { get; set; }
    }
}
