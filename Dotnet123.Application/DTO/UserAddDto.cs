﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dotnet123.Application.DTO
{
    public class UserAddDto
    {
        [Required]
        [MinLength(5)]
        [MaxLength(10)]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
