﻿using dotnet123.Model;

namespace Dotnet123.IRepository
{
    public interface IUserRepository
    {
        public bool UserAdd(tb_User model);

        /// <summary>
        /// 定义用户登录接口
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UserLogin(System.Linq.Expressions.Expression<Func<tb_User,bool>> user);//fun是有返回值的委托，这里的in是协变
       
        
    }
}