const {defineConfig} = require('@vue/cli-service')
 
module.exports = defineConfig({
    devServer: {
        host: 'localhost',
        port: 3000,  // 启动端口号
        proxy: {
            '/api': { // 请求接口中要替换的标识
                target: 'http://117.62.22.235:17009', // 代理地址
                ChangeOrigin: true, // 是否允许跨域
                secure: true,
                pathRewrite: {
                    '^/api': '' // 这里理解成用‘/api’代替target里面的地址，后面组件中我们掉接口时直接用api代替 比如我要调用'http://40.00.100.100:3002/user/add'，直接写‘/api/user/add’即可
                }
            }
        }
    }
})