using Microsoft.AspNetCore.Mvc;
using Net6.DbContext;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Net6.DbContext.Models;

namespace net6中调用存储过程.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
       private readonly Net6DbContext _dbContext;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, Net6DbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        [HttpGet]
        public void Get()
        {
          //  SqlParameter[] queryPara = new SqlParameter[] {
          //  new SqlParameter("@id",1)
          //  };

         // IQueryable<tb_user_view> res=  _dbContext.tb_user_view.FromSqlRaw("exec sp_demo_list @id", queryPara);
          //  //看看跟踪有没有执行sql 

          //  //采用EF执行删除操作，看看sql执行情况

          //  var users = _dbContext.tb_Staff.Where(x => x.Id > 500&&x.Id<600);
          //  _dbContext.RemoveRange(users);
          //  _dbContext.SaveChanges();

          //  var list=res.ToList();//看看35行数据现在出来没有,避免过早的toList

          //  //采用原生的sql执行删除,查看他们之间的区别，重点，好好看
            _dbContext.Database.ExecuteSqlRaw("delete from tb_staff where id>700 and id<800");


            //调用扩展方法执行存储过程
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("@staffName", "张三");
            //设置参数为返回参数
            SqlParameter returnPara= new SqlParameter("@res",System.Data.SqlDbType.Text);
            returnPara.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters[1] = returnPara;
            //调用扩展方法:
            _dbContext.ExecuteProcedureReturnValue("sp_demo", parameters);
            //获取返回值
            var resData = returnPara.Value;

         
        }
    }
}