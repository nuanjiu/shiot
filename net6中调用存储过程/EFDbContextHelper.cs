﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Net6.DbContext;
using System.Runtime.CompilerServices;

namespace net6中调用存储过程
{
    public static class EFDbContextHelper
    {

        public static  bool ExecuteProcedureReturnValue(this DbContext net6DbContext,string proceNuame, SqlParameter[]? parameters=null)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(net6DbContext.Database.GetConnectionString()))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand(proceNuame, conn);
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    command.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
