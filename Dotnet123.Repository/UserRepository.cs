﻿using dotnet123.Model;
using Dotnet123.EFCore;
using Dotnet123.IRepository;
using System.Linq.Expressions;

namespace Dotnet123.Repository
{
    public class UserRepository : IUserRepository
    {
        public readonly BusinessDbContext _businessDbContext;
        public UserRepository(BusinessDbContext businessDbContext)
        {
            _businessDbContext = businessDbContext;
        }

        public bool UserAdd(tb_User model)
        {
            try
            {
                _businessDbContext.Add(model);
                _businessDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.Message);
                
            }
        }
        /// <summary>
        /// 实现用户登录
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool UserLogin(Expression<Func<tb_User, bool>> user)
        {
            return _businessDbContext?.tb_Users?.First(user) != null;
        }
    }
}