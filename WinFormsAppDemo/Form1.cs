namespace WinFormsAppDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

      
        private void Form1_Load(object sender, EventArgs e)
        {
          

        }

      



       

        private void btn_open_Click(object sender, EventArgs e)
        {
           OpenFileDialog openFileDialog=new OpenFileDialog();
           DialogResult dialogResult= openFileDialog.ShowDialog();
            if(dialogResult==DialogResult.OK)
            {
                this.txtFileName.Text = openFileDialog.FileName;
            }
        }

        private void btn_create_Click(object sender, EventArgs e)
        {
            string OriginFile=this.txtFileName.Text;
            string DstFile = OriginFile.Replace(".", "a.");
            string strCmd = " -ss " + this.txt_beginTime.Text
                + " -i " + OriginFile
                + " -to " + this.txt_endTime.Text
                + " -vcodec copy -acodec copy " + DstFile + " -y ";
            if (System.IO.File.Exists(DstFile)) System.IO.File.Delete(DstFile);
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "ffmpeg.exe";//要执行的程序名称
            p.StartInfo.Arguments = " " + strCmd;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = false;//可能接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = false;//由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = false;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = false;//不显示程序窗口

            p.Start();//启动程序
            p.WaitForExit();//等待程序执行完退出进程

        }
    }
}