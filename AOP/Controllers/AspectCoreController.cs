﻿using AOP.AspectCore_框架_;
using AOP应用.装饰器;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AOP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize()]
    public class AspectCoreController : ControllerBase
    {
        IUserService _userServiceuserService;
        public AspectCoreController(IUserService userServiceuserService)
        {
            _userServiceuserService = userServiceuserService;
        }
        [HttpGet]
        public void AdpectCoreTest()
        {
            _userServiceuserService.UserLogin("admin","123456");

           
        }
    }
}
