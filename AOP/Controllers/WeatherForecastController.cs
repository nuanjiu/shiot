using AOP.代理_Proxy_;
using AOP应用.装饰器;
using Microsoft.AspNetCore.Mvc;

namespace AOP.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
   
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IHttpContextAccessor _httpContext;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IHttpContextAccessor httpContext)
        {
            _logger = logger;
            _httpContext = httpContext;
        }

        [HttpGet]
        public string Get()
        {
            UserDecorateor.Main(_httpContext);
            UserProxy.Main(_httpContext);
            return "返回结果成功";
        }
    }
}