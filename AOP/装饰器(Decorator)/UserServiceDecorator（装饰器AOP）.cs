﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AOP应用.装饰器
{
    public class UserServiceDecorator : IUserService
    {
        private  IUserService _userService { get; set; }

        private IHttpContextAccessor _httpContext { get; set; }
        public UserServiceDecorator(IUserService userService, IHttpContextAccessor httpContext)
        {
            _userService = userService;
            _httpContext = httpContext;
        }
        public string UserLogin(string userName, string userPassword)
        {
            Before();//方法执行之前进行日志存储
            var res = _userService.UserLogin(userName, userPassword);
            After(res);//记录方法的执行结果
            return res;
        }

        /// <summary>
        /// 方法执行之前
        /// </summary>
        private bool Before()
        {
            var ip = _httpContext?.HttpContext?.Request.Host.ToString();
            var browser = _httpContext?.HttpContext?.Request.Headers["browser"].ToString();
            var path = _httpContext?.HttpContext?.Request.Path;
            //获取数据写入到数据库
            Console.WriteLine($"装饰器模式-把数据写入到数据库,ip={ip},browser={browser},path={path}");
            return true;
        }
        private bool After(string result)
        {

            //获取操作数据写入到数据库
            Console.WriteLine($"装饰器模式-把数据写入到数据库,执行结果是={result}");
            return true;
        }
    }
}
