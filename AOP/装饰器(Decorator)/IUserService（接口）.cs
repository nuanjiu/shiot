﻿using AOP.AspectCore_框架_;
using AspectCore.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOP应用.装饰器
{
    public interface IUserService
    {
        public string UserLogin(string userName, string userPassword);
    }
}
