﻿using AspectCore.DynamicProxy;
using Microsoft.Extensions.Caching.Memory;

namespace AOP.AspectCore_框架_
{
    public class MemercacheFilter : AbstractInterceptorAttribute
    {
     static IMemoryCache _memoryCache = new MemoryCache(new MemoryCacheOptions());

      
        /// <summary>
        ///     实现抽象方法
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        public override async Task Invoke(AspectContext context, AspectDelegate next)
        {
            try
            {
              
                Console.WriteLine("执行之前");
                var res = _memoryCache.Get("c_user_cache");
                if (res == null)
                {
                    await next(context);//执行被拦截的方法
                    _memoryCache.Set("c_user_cache", context.ReturnValue,System.DateTime.Now.AddMinutes(5));
                    Console.WriteLine("缓存数据");
                }
                if(res!=null)
                {
                    Console.WriteLine("从缓存中读取数据");
                }

            }
            catch (Exception)
            {
                Console.WriteLine("被拦截的方法出现异常");
                throw;
            }
            finally
            {
                Console.WriteLine("执行之后");
            }
        }
    }

}
