using AOP.AspectCore_���_;
using AOPӦ��.װ����;
using AspectCore.Configuration;
using AspectCore.Extensions.DependencyInjection;
using AspectCore.Extensions.Hosting;
using Microsoft.Extensions.Caching.Memory;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddTransient<IHttpContextAccessor,HttpContextAccessor>();
builder.Services.AddTransient<IMemoryCache, MemoryCache>();
builder.Services.AddTransient<IUserService,UserService>();

builder.Services.ConfigureDynamicProxy(config => {
config.Interceptors.AddTyped<MemercacheFilter>();
});
builder.Services.AddMvc();
builder.Services.AddMemoryCache();
builder.Host.UseDynamicProxy();


var app = builder.Build();


// Configure the HTTP request pipeline.

app.UseAuthorization();


app.MapControllers();

app.Run();
