﻿namespace AOP.代理_Proxy_
{
    public class UserProxy
    {
        public static void Main(IHttpContextAccessor _iHttpContextAccessor)
        {
            UserServiceProxy userServiceProxy = new UserServiceProxy(_iHttpContextAccessor);
            userServiceProxy.Login("admin", "123456");

        }
    }
}
