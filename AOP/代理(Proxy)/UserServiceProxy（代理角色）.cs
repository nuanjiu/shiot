﻿using System.Net.NetworkInformation;

namespace AOP.代理_Proxy_
{
    public class UserServiceProxy : IUserService
    {
        private IHttpContextAccessor _httpContext { get; set; }

        private IUserService _userService = new UserService();
        public  UserServiceProxy(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }
       

    
        /// <summary>
        /// 方法执行之前
        /// </summary>
        private bool Before()
        {
            var ip = _httpContext?.HttpContext?.Request.Host.ToString();
            var browser = _httpContext?.HttpContext?.Request.Headers["browser"].ToString();
            var path = _httpContext?.HttpContext?.Request.Path;
            //获取数据写入到数据库
            Console.WriteLine($"代理模式-把数据写入到数据库,ip={ip},browser={browser},path={path}");
            return true;
        }
        private bool After(string result)
        {

            //获取操作数据写入到数据库
            Console.WriteLine($"代理模式-把数据写入到数据库,执行结果是={result}");
            return true;
        }

        public string Login(string userName, string userPassword)
        {
            Before();
            string res = _userService.Login(userName, userPassword);
            After(res);
            return res;
        }
    }
}
