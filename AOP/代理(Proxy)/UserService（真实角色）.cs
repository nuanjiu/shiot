﻿namespace AOP.代理_Proxy_
{
    public class UserService : IUserService
    {
        public string Login(string userName, string userPassword)
        {
          return this.UserLogin(userName, userPassword);
        }

        public string UserLogin(string userName, string userPassword)
        {
            if (userName == "admin" && userPassword == "123456")
            {
                return "登录成功";
            }
            else
            {
                return "登录失败";
            }
        }
    }
}
