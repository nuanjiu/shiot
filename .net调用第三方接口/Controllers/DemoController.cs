﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplicationApi.Controllers
{
    [Route("/mpp/produce/[action]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        [HttpPost]
        public IActionResult checkFromOut(ParamterDto dto)
        {
            //处理业务逻辑
            /*被请求方（提供接口的一方）
             
             接收请求方传递过来的数据,拿 到数据之后
            mes系统拿到hr的数据之后，他肯定要把这个数据同步到mes系统。
            如果mes系统同步完成之后，你要告诉对方数据处理完成
            然后就记录目志。
            第三方接口的调用：一定要注意以下的几个方面：
            1）参数的有效性，如果你的参数不符合接口提供方的要求，肯定传不过来。
            2）要保证数据的一致性，如果你调用了第三方接口，拿到数据之后并且处理完成了，你要告诉对方，对方同步处理。
            3）所以我们调用第三方接口时一定要记录日志，如果我mes系统同步处理完了一部门hr的人员数据，你应通知hr系统处理成功（有
            一种情况你通知他了，他没有处理或是处理失败），这时候你再调用肯定会数据重复。
            你不要相信对方会正常处理，而是要在自己的系统里做好处理结的标识。
            mes系统调hr系统的人员数据(id为100001的数据，新增)，你处理完成之后，要先在自己系统的日志表里记录10001这种数据已经处理了。
             */
            return Ok(new { data="",msg="数据处理成功",code=200});
        }
    }
}
