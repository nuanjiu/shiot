using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace WebApplicationApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public  string Get()
        {
            //
            var p = new ParamterDto();
            p.userId = "userid";
            p.content = new Content() {
                barcode = "a",
                productCode="b",
                 stationSeq=3
            };
            
            var handler = new HttpClientHandler();
            handler.SslProtocols =System.Security.Authentication.SslProtocols.Tls11  ;
            var httpClient = new HttpClient(handler);
            //所有https协议的接口地址都要在httpclient里通过构造函数来调用httpclienthandler

          //  HttpClient httpClient = new HttpClient();//http对象
            //表头参数
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //在请求头里标识使用的是json


            //传参数
            HttpContent httpContent =JsonContent.Create(p,typeof(ParamterDto),MediaTypeHeaderValue.Parse("application/json"), new System.Text.Json.JsonSerializerOptions() { 
             Encoder=System.Text.Encodings.Web.JavaScriptEncoder.Default
            });
            //请求
            HttpResponseMessage response = httpClient.PostAsync("https://localhost:7188/mpp/produce/checkFromOut", httpContent).Result;
            if (response.IsSuccessStatusCode)
            {
                Task<string> t =  response.Content.ReadAsStringAsync();
                //拿到第三方的接口返回的数据，肯定是要做我们自己的业务
                if (t != null)
                {
                    return t.Result;
                }
            }
            return string.Empty;
        }
    }
    //public class JsonContent : StringContent
    //{
    //    public JsonContent(object obj) :
    //       base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")
    //    { }
    //}
}