﻿using System.ComponentModel.DataAnnotations;

namespace WebApplicationApi
{
    public class ParamterDto
    {
        [Required]
        public string userId { get; set; }
        public Content content { get; set; }

    }

    public class Content
    {
        [Required]
        [MaxLength(20)]
        public string productCode { get; set; }
        public string barcode { get; set; }
        public int stationSeq { get; set; }
    }
}
