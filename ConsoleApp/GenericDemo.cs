﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConsoleApp
{
    /// <summary>
    /// 父类
    /// </summary>
    public class Parent
    {
        public int id;
        public void Test()
        {
            Console.WriteLine("parent.test");
        }
    }
    /// <summary>
    /// 子类
    /// </summary>
    public class Children : Parent
    {
        public int cid { get; set; }

        public void TestChildren()
        {
            Console.WriteLine("children.test");


        }
    }
    public interface IParent
    {
        void Test();
    }
    internal class GenericDemo
    {

        public static void Detail<T>(T para) where T : Parent//基类约束
        {
            para.Test();

        }
        public static void DetailChild<T>(T para) where T : Children//基类约束
        {
            para.Test();
            para.TestChildren();

        }
        public static void Detail2<T>(T para) where T : struct//结构体约束/值类型约束
        {
            Console.WriteLine(para.ToString());
        }

        public static void Detail3<T>(T para) where T : class//引用约束/类约束
        {
            Console.WriteLine(para.GetType().Name);
        }

        public static void Detail4<T>(T para) where T : IParent//接口约束
        {
            para.Test();
        }
        public static void Detail5<T>(T para) where T : new()//new 约束
        {
            para = new T();
        }
    }

    public interface IListOut<out  T>
    {
        T? InitDefault();
       // T ShowDetail(T t);

    }
    public class ListOut<T> : IListOut<T>
    {
        public T? InitDefault()
        {
            return default(T);
        }

        public T ShowDetail(T t)
        {
            return t;
        }
    }

    public interface IListIn<in T>
    {
       // T? InitDefault();
       // T ShowDetail(T t);

    }
    public class ListIn<T> : IListIn<T>
    {
        public T? InitDefault()
        {
            return default(T);
        }

        public T ShowDetail(T t)
        {
            return t;
        }
    }
    public class Main
    {
        public void Run()
        {

            Children c = new Children();
            c.TestChildren();
            c.cid = 1;

            /*
             System.Reflection 命名空间中的类与 System.Type 使你能够获取有关加载的程序集和其中定义的类型的信息，如类、接口和值类型（即结构和枚举）。 可以使用反射在运行时创建、调用和访问类型实例
             
             */

            Parent p = new Parent();
            Parent p1 = new Children();
            //Children c1 = new Parent();
            //协变
            IListOut<Parent> list = new ListOut<Children>();

            IListOut<Parent> list1 = new ListOut<Parent>();
            //逆变
            IListIn<Children> list3 = new ListIn<Children>();
            IListIn<Children> list4 = new ListIn<Parent>();
            //IEnumberable本来就可协变
            IEnumerable<Children> childrens= new List<Children>();
            IEnumerable<Parent> parents = new List<Children>();
            //action采用in 逆变
            Action<Parent> b = (target) => { Console.WriteLine(target.GetType().Name); };
            Action<Children> d = b;
            d(new Children());
          

        }
    }



}
