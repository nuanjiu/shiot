﻿// See https://aka.ms/new-console-template for more information
using ConsoleApp;
using System.Reflection;


Console.WriteLine("Hello, World!,一个不正经的程序员，让你在工作中也能和同事有点可聊的");

new ThreadDemo().ThreadSafe();

#region 反射


//DataTableToDynamics.Main();




//Run.Main();
//GenericDemo.Detail3<string>("张三");
//ReflectionDemo.Main();

//C# Assembly 反射动态加载程序集(动态加载Dll)    
//演示怎么通过dll来反射应用程序集来，执行sql
var assembly = Assembly.LoadFrom("DatabaseDll\\DataSource.Lib.dll");//加载应用程序集

//if (assembly != null)
//{
//    Type? type = assembly.GetType("DataSource.Lib.DataBaseMSSqlDemo");//如果数据库是mssql
//    if (type != null)
//    {
//        var entity = Activator.CreateInstance(type, true);
//        MethodInfo? m = type.GetMethod("ExecuteSql");
//        var res = m?.Invoke(entity, new object[] { "select * from table" });
//    }

//    type = assembly.GetType("DataSource.Lib.DataBaseMySqlDemo");//如果数据库是mysql
//    if (type != null)
//    {
//        var entity = Activator.CreateInstance(type, true);
//        MethodInfo? m = type.GetMethod("ExecuteSql");
//        var res = m?.Invoke(entity, new object[] { "select * from table" });
//    }

//} 
#endregion
