﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{

    public class Demo
    {

        public Demo() { }

       

        public Demo(int id)
        {
            this.id = id;
        }
        public int id { get; set; }
        public string Name { get => name; set => name = value; }

        private string name;

        private void Test()
        {

        }
        public void Test(int id)
        {
            Console.WriteLine("id");
        }
        public string Test(int id,string name)
        {
            return $"id={id},name={name}";
        }
        public  static void TestStatic(int id)
        {
            Console.WriteLine("id");
        }
    }

    public static class ReflectionDemo
    {

        public static void Main()
        {

         //  var a= Assembly.LoadFile("C:\\Users\\Administrator\\source\\repos\\课件\\ConsoleApp\\bin\\Debug\\net6.0\\ConsoleApp.dll");
        

            Demo d = new Demo();
            d.Test(1, "张三");

            Demo.TestStatic(1);

            Demo d1 = new Demo(1);


            Type demo = typeof(Demo);

           

            ConstructorInfo[] constructors = demo.GetConstructors();//获取构造函数
            MethodInfo[] methodInfos = demo.GetMethods();
            FieldInfo[] fieldInfos = demo.GetFields();
            object oGeneric = Activator.CreateInstance(demo, true);//相关当于new 一个实例

            var flags = BindingFlags.InvokeMethod | BindingFlags.Instance |
     BindingFlags.Public | BindingFlags.Static;
            var args = new object[] { 42 };
            //调用静态方法
            MethodInfo m = demo.GetMethod("TestStatic");
             m.Invoke(null, args);
            //调用重载方法
             m = demo.GetMethod("Test", new Type[] { typeof(int) });
            m.Invoke(oGeneric, args);
            m = demo.GetMethod("Test", new Type[] { typeof(int),typeof(string)});
            args = new object[] { 42,"张三" };
          object ret=  m.Invoke(oGeneric, args);

            object active = Activator.CreateInstance(demo,12);
            m = demo.GetMethod("Test", new Type[] { typeof(int), typeof(string) });
            m.Invoke(active, args);

            //datatable转list
            DataTable data = new DataTable();
            data.Columns.Add(new DataColumn() { ColumnName = "id", DataType = typeof(int) });
            data.Columns.Add(new DataColumn() { ColumnName = "Name", DataType = typeof(string) });
            DataRow dr = data.NewRow();
            dr["id"] = 1;
            dr["name"] = "张三";
            data.Rows.Add(dr);
            dr = data.NewRow();
            dr["id"] = 1;
            dr["name"] = "李四";
            data.Rows.Add(dr);

            List<Demo> res = DataListHepler.ConvertToEx<Demo>(data);


        }

    }



    public class DataListHepler
    {
        public static List<T> ConvertToEx<T>(DataTable dt) where T : new()
        {
            if (dt == null) return null;
            if (dt.Rows.Count <= 0) return null;

            List<T> list = new List<T>();
            Type type = typeof(T);
            PropertyInfo[] propertyInfos = type.GetProperties();  //获取泛型的属性
            List<DataColumn> listColumns = dt.Columns.Cast<DataColumn>().ToList();  //获取数据集的表头，以便于匹配
            T t;
            foreach (DataRow dr in dt.Rows)
            {
                t = new T();
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    try
                    {
                        //修复可以为空的bug
                        DataColumn? dColumn = listColumns?.Find(name => name.ToString().ToUpper() == propertyInfo?.Name.ToUpper());  //查看是否存在对应的列名

                        if (dColumn != null)
                        {
                            /*
                               var uu = propertyInfo.PropertyType.Name;
                            if (uu == "Int32")
                             */
                            var uu = propertyInfo.PropertyType;
                            if (uu == typeof(int))
                            {
                                if (dr[propertyInfo.Name].ToString() != "")
                                {
                                    propertyInfo.SetValue(t, Convert.ToInt32(dr[propertyInfo.Name]), null);  //赋值
                                }
                            }
                            else if (uu ==typeof(bool))
                            {
                                if (dr[propertyInfo.Name].ToString() != "")
                                {
                                    propertyInfo.SetValue(t, Convert.ToBoolean(dr[propertyInfo.Name]), null);  //赋值
                                }
                            }
                            else if (uu == typeof(decimal))
                            {
                                if (dr[propertyInfo.Name].ToString() != "")
                                {
                                    propertyInfo.SetValue(t, Convert.ToDecimal(dr[propertyInfo.Name]), null);  //赋值
                                }
                            }
                            else if (uu == typeof(Demo))
                            {
                                if (dr[propertyInfo.Name].ToString() != "")
                                {
                                    propertyInfo.SetValue(t, Convert.ToDouble(dr[propertyInfo.Name]), null);  //赋值
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(dr[propertyInfo.Name].ToString()))
                                {
                                    propertyInfo.SetValue(t, dr[propertyInfo.Name], null);  //赋值
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                list.Add(t);
            }
            return list;
        }
    }
}
