﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Net.Http.Json;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ConsoleApp
{

    public static class DataTableToDynamics
    {
        public  static void Main()
        {
            //定认模拟数据
            DataTable data = new DataTable();
            data.Columns.Add(new DataColumn() { ColumnName = "id", DataType = typeof(int) });
            data.Columns.Add(new DataColumn() { ColumnName = "Name", DataType = typeof(string) });
            DataRow dr = data.NewRow();
            dr["id"] = 1;
            dr["name"] = "张三";
            data.Rows.Add(dr);
            dr = data.NewRow();
            dr["id"] = 2;
            dr["name"] = "李四";
            data.Rows.Add(dr);
            //获取DataTable的列集体信息
            Dictionary<string, Type> columns = new Dictionary<string, Type>();
            foreach (DataColumn column in data.Columns)
            {
                columns.Add(column.ColumnName, column.DataType);
            }
            List<dynamic> res = new List<dynamic>();//用于接收数据
            foreach (DataRow dataRow in data.Rows)//处理每一行数据
            {
                var m = new Dictionary<string, object>();//用于接收每一行的数据

                foreach (var item in columns)
                {
                   
                    if (dataRow[item.Key] != DBNull.Value && dataRow[item.Key] != null)//如果数据不为空，对列进行循环把数据给到字典
                    {
                        m[item.Key] = dataRow[item.Key];
                    }
                    else//处理默认值
                    {
                        Type type = item.Value;
                        if (type == typeof(Int32))
                            m[item.Key] = 0;
                        if (type == typeof(string))
                            m[item.Key] = "";
                        if (type == typeof(Double))
                            m[item.Key] = 0.0;
                        if (type == typeof(float))
                            m[item.Key] = 0;
                        if (type == typeof(Decimal))
                            m[item.Key] = 0;
                        if (type == typeof(bool))
                            m[item.Key] = false;
                      

                    }
                   
                }
                res.Add(m);
            }
            string json= JsonConvert.SerializeObject(res);//这里一般在前台api可以转一下
            Console.WriteLine(json);
        }
       
    }
}
