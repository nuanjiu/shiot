﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class ThreadDemo
    {
        /*线程池*/
        public void ThreadPoolTest()

        {
            ThreadPool.SetMaxThreads(16, 16);
            ThreadPool.SetMinThreads(8, 8);
            int workerThreads = 0;
            int completionPortThreads = 0;
            ThreadPool.GetMaxThreads(out workerThreads, out completionPortThreads);
            ThreadPool.GetMinThreads(out workerThreads, out completionPortThreads);

            Stopwatch watch = Stopwatch.StartNew();
            watch.Start();
            WaitCallback callback = index =>
            {
                Console.WriteLine($"在{watch.Elapsed} 毫秒:线程 {index} 启动成功");
            };

            for (int i = 0; i < 20; i++)
            {
                ThreadPool.QueueUserWorkItem(callback, i);
            }
        }

        private static object lockObj = new object();
        private int count = 0;
        public void ThreadSafe()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            #region 互斥锁
            //Thread thread = new Thread(AddCount);
            //thread.Start();
            //Thread thread1 = new Thread(AddCount);
            //thread1.Start();
            //thread.Join();
            //thread1.Join(); 
            #endregion
            #region Monitor
            //Thread thread = new Thread(AddCountMonitor);
            //thread.Start();
            //Thread thread1 = new Thread(AddCountMonitor);
            //thread1.Start();
            //thread.Join();
            //thread1.Join();
            #endregion

            #region 使用spinlock
            //Thread thread = new Thread(AddCountSpinlock);
            //thread.Start();
            //Thread thread1 = new Thread(AddCountSpinlock);
            //thread1.Start();
            //thread.Join();
            //thread1.Join();
            #endregion

            #region 使用spinlock
            Thread thread = new Thread(AddCountMutex);
            thread.Start();
            Thread thread1 = new Thread(AddCountMutex);
            thread1.Start();
            thread.Join();
            thread1.Join();
            #endregion

            stopwatch.Stop();
            Console.WriteLine($"count={count},耗时:{stopwatch.ElapsedMilliseconds}");
            this.SemaphoreTest();//测试线程量
            Console.ReadLine();
        }
        #region 互斥锁
        /*使用互斥锁*/
        public void AddCount()
        {
            // lock (this)//锁定是的当前对像
            // {
            for (int i = 0; i < 100000000; i++)
            {
                // lock (lockObj)//加lock只能锁object对象，注意一下，锁定代码块
                //  {
                // count++;
                Interlocked.Increment(ref count);
                // }
                //  }
            }

        }
        #endregion
        #region 使用Monitor
        /// <summary>
        /// 使用Monitor
        /// </summary>
        public void AddCountMonitor()
        {
            // Monitor.Enter(lockObj);//lock实际就是Monitor的语法糖，实际的实现和Monitor一样。他与lock不一样的地方是可以设置超时时间
            bool lockTaken = false;
            while (lockTaken == false)
                Monitor.TryEnter(lockObj, 20, ref lockTaken);//这两行就是设置这个超时时间是500毫秒，lockTaken这个是如果获取到锁，返回true,如果未获取，返回false
            if (lockTaken)
            {
                try
                {
                    for (int i = 0; i < 100000000; i++)
                    {
                        count++;
                    }
                }
                finally
                {
                    Monitor.Exit(lockObj);
                }
            }

        }
        #endregion
        #region 使用Spinlock
        /// <summary>
        /// 使用自旋锁
        /// </summary>

        private static SpinLock spinLock = new SpinLock();
        public void AddCountSpinlock()
        {
            bool lockTaken = false;
            while(!lockTaken)
            spinLock.TryEnter(ref lockTaken);//lockTaken这个是如果获取到锁，返回true,如果未获取，返回false
            if (lockTaken)
            {
                try
                {
                    for (int i = 0; i < 100000000; i++)
                    {
                        count++;
                    }
                }
                finally
                {
                    spinLock.Exit();
                }
            }

        }
        #endregion

        #region 使用Mutex
        /// <summary>
        /// 使用Mutex
        /// </summary>

        bool createNew = false;
        
        public void AddCountMutex()
        {

            Mutex mutex = new Mutex(false, "mutex.createnew",out createNew);
            //第二个参数指定互斥锁的名字
           
            if (mutex.WaitOne())
            {
                try
                {
                    for (int i = 0; i < 100000000; i++)
                    {
                        count++;
                    }
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            }

        }
        #endregion


        #region 信号量
        public void SemaphoreTest()
        {
            Semaphore semaphore = new Semaphore(2, 4);
            for (int i = 0; i < 10; i++)
            {
                var count = i + 1;
                Task.Run( async () =>{
                    semaphore.WaitOne();
                    Console.WriteLine($"线程开始下载{count}文件");
                    //模拟下载时间
                    var r = new Random();
                    await Task.Delay(r.Next(5000,10000));
                    Console.WriteLine($"线程下载{count}文件完成");
                    semaphore.Release();

                });
            }

        } 

     
        #endregion
    }
}
