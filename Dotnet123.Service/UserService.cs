﻿using dotnet123.Model;
using Dotnet123.Application.DTO;
using Dotnet123.IRepository;
using Dotnet123.IService;

namespace Dotnet123.Service
{

    public class UserService : IUserService
    {
        public readonly IUserRepository _iUserRepository;

        public UserService(IUserRepository iUserRepository)
        {
            _iUserRepository = iUserRepository;
        }
        public bool RegUserAdd(UserAddDto user)
        {
            tb_User model = new tb_User();
            model.UserName = user.UserName;
            model.Password = user.Password;
            return _iUserRepository.UserAdd(model);
        }

        /// <summary>
        /// 实现用户登录
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public bool UserLogin(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName))
                throw new Exception("用户名不能为空");
            if (string.IsNullOrEmpty(password))
                throw new Exception("用户密码不能为空");
            System.Linq.Expressions.Expression<Func<tb_User,bool>> func=x=>x.UserName==userName&&x.Password==password&&x.IsDeleted==false;//构造表达式，后来可以做成动态的
            return _iUserRepository.UserLogin(func);
        }
    }
}

