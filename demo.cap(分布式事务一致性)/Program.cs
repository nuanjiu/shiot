var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddCap(c =>
{
    c.UseSqlServer(@"Data Source=.;Initial Catalog=cap_demo;User ID=sa;Password=123456;Encrypt=True;TrustServerCertificate=True;"); //使用SqlServer数据库,连接地址请依实际修改
    c.UseRabbitMQ(mq =>
    {
        mq.HostName = "127.0.0.1"; //RabitMq服务器地址，依实际情况修改此地址
        mq.Port = 5672;
        mq.UserName = "guest";  //RabbitMq账号
        mq.Password = "guest";  //RabbitMq密码
    });
    c.UseDashboard();
});

builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
