using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;
using WmsServiceReference;

namespace demo.cap.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
       

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<string> Get([FromServices] ICapPublisher capPublish)
        {
            //订单创建业务处理完成。

            WMSWebServiceSoapClient wMSWebServiceSoapClient = new WMSWebServiceSoapClient(WMSWebServiceSoapClient.EndpointConfiguration.WMSWebServiceSoap);
         Product[] products=  wMSWebServiceSoapClient.GetDataByOrderId("1000");
            await capPublish.PublishAsync<Product[]>("Order.Created", products);  //发布Order.Created事件
            return "订单创建成功啦";
        }

        [NonAction]
        [CapSubscribe("Order.Created")]    //监听Order.Created事件
        public async Task OrderCreatedEventHand(Product[] msg)
        {

            await Task.Run(() => { Console.WriteLine(msg); });
        }
    }
}