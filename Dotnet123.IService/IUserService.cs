﻿using Dotnet123.Application.DTO;

namespace Dotnet123.IService
{
    public interface  IUserService
    {
        public bool RegUserAdd(UserAddDto user);

        /// <summary>
        /// 用户登录接口
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        public bool UserLogin(string userName,string password);

    }
}