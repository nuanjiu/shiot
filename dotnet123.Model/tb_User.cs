﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dotnet123.Model
{
    [Table("tb_user")]
    public class tb_User : BaseEntity
    {
        [Required]
        [MinLength(5)]
        [MaxLength(20)]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public string? Email { get; set; }
        public string? Mobile { get; set; }

        public string? EmailVerified { get; set; }

        public int? Sex { get; set; }

        public string? Photo { get; set; }

        public string? Remark { get; set; }

        public string? OpenID { get; set; }
    }
}