﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace dotnet123.Model
{
    /// <summary>
    /// 审计与扩展字段
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// 唯一ID
        /// </summary>
        public string Id { get; set; } = new Snowflake.Core.IdWorker(1, 1).NextId().ToString();//注意这个工作中心id与databaseid在分布式下不能写死

        public string? CreateId { get; set; }
        public DateTime? UpdateId { get; set; }
        public DateTime CreateDate { get; set; } = System.DateTime.Now;
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; } = false;

        public string? ext001 { get; set; }//冗余备用字段，dapper

        public string? ext002 { get; set; }

        public int? ext003 { get; set; }

        public int? ext004 { get; set; }
    }
}
