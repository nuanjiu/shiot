﻿using Dotnet123.Application.DTO;
using Dotnet123.IService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.NetworkInformation;

namespace DotNet123.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public bool Reg(UserAddDto userAddDto)
        {
            return _userService.RegUserAdd(userAddDto);
        }
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="userName">登录用户名</param>
        /// <param name="password">登录密码</param>
        /// <returns></returns>
        [HttpPost("user_login")]
        public bool UserLogin(string userName,string password)
        {
            return _userService.UserLogin(userName, password);
        }
    }




}

