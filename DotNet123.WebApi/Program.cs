using Dotnet123.EFCore;
using Dotnet123.IRepository;
using Dotnet123.IService;
using Dotnet123.Repository;
using Dotnet123.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<BusinessDbContext>(x =>
{
    string? conn = builder.Configuration?.GetConnectionString("defaultConn");
    if (!string.IsNullOrEmpty(conn))
        x.UseMySql(conn, ServerVersion.AutoDetect(conn));
    else
        throw new Exception("���ݿ������ַ���Ϊ��");

});

builder.Services.AddControllers();

builder.Services.AddTransient<IUserRepository, UserRepository>();
builder.Services.AddTransient<IUserService, UserService>();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

app.Run();
