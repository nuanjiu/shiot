﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 线程安全集合
{
    public class ConcurrentQueueDemo
    {
        public static void Run(int type = 0)
        {
            if (type == 0)
            {
                #region 队列
                object lockObj = new object();
                var num = 0;
                Queue<int> queue = new Queue<int>();
                Task.Factory.StartNew(() =>
                {

                    for (int i = 0; i < 10000; i++)
                    {

                        queue.Enqueue(num++);
                        Console.WriteLine($"Task进程执行入队操作:" + num);


                    }

                });
                for (int i = 0; i < 10000; i++)
                {

                    queue.Enqueue(num++);
                    Console.WriteLine($"主进程执行入队操作:" + num);

                }
                bool isDequeue = true;
                int res = 0;
                while (isDequeue)
                {
                    isDequeue = queue.TryDequeue(out res);
                    if (res > 0)
                    {
                        Console.WriteLine("出队数据：" + res);
                    }
                }
                #endregion
            }
            if (type == 1)
            {
                #region 队列
                object lockObj = new object();
                var num = 0;
                Queue<int> queue = new Queue<int>();
                Task.Factory.StartNew(() =>
                {

                    for (int i = 0; i < 10000; i++)
                    {
                        lock (lockObj)
                        {
                            queue.Enqueue(num++);
                            Console.WriteLine($"Task进程执行入队操作:" + num);
                        }

                    }

                });
                for (int i = 0; i < 10000; i++)
                {
                    lock (lockObj)
                    {
                        queue.Enqueue(num++);
                        Console.WriteLine($"主进程执行入队操作:" + num);
                    }
                }
                bool isDequeue = true;
                int res = 0;
                while (isDequeue)
                {
                    isDequeue = queue.TryDequeue(out res);
                    if (res > 0)
                    {
                        Console.WriteLine("出队数据：" + res);
                    }
                }
                #endregion
            }
            if (type == 2)
            {
                #region 线程安全队列
                var num_c = 0;
                ConcurrentQueue<int> queue_c = new ConcurrentQueue<int>();
                Task.Factory.StartNew(() =>
                {

                    for (int i = 0; i < 10000; i++)
                    {
                        queue_c.Enqueue(num_c++);
                        Console.WriteLine($"Task进程执行入队操作:" + num_c);
                    }
                });
                for (int i = 0; i < 10000; i++)
                {
                    queue_c.Enqueue(num_c++);
                    Console.WriteLine($"主进程执行入队操作:" + num_c);
                }

                bool isDequeue = true;
                int res = 0;
                while (isDequeue)
                {
                    isDequeue = queue_c.TryDequeue(out res);
                    if (res > 0)
                    {
                        Console.WriteLine("出队数据：" + res);
                    }
                }
                #endregion
            }
        }
    }
}
