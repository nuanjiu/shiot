﻿using NPOI.HSSF.UserModel;
using NPOI.OpenXmlFormats.Spreadsheet;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace 线程安全集合
{
    public class ExcelOperation
    {
        public static BlockingCollection<ProjectModel>? blockingCollection;//在添加或是读取元素之前，会阻塞进程一直等待
        public static ConcurrentQueue<BlockingCollection<ProjectModel>> sheets = new ConcurrentQueue<BlockingCollection<ProjectModel>>();
        static Stopwatch stopwatch = new Stopwatch();
        public static void Run()
        {
            Console.Write("开始导出数据");
            stopwatch.Start();
            /*先看下npoi操作excel的基础步骤*/
            //  ExcelDemo();

            new DataAccess().GetData();
            new DataAccess().CreateExcel();

        }

        public static void ExcelDemo()
        {
            XSSFWorkbook _workbook = new XSSFWorkbook();//存120多万

            ISheet sheet = _workbook.CreateSheet("Sheet1");
            IRow row = sheet.CreateRow(0);//创建0行
            row.CreateCell(0).SetCellValue("第0行第一列");
            row.CreateCell(2).SetCellValue("第0行第二列");
            row = sheet.CreateRow(1);//创建1行
            row.CreateCell(0).SetCellValue("第1行第一列");
            row.CreateCell(2).SetCellValue("第1行第二列");
            FileStream fileStream = new FileStream($"{System.DateTime.Now.Ticks}.xls", FileMode.CreateNew);//创建一个文件流
            _workbook.Write(fileStream);//保存文件
            fileStream.Close();//关闭流

        }

        public class ProjectModel
        {
            public string? Id { get; set; }
            public string? ProjectCode { get; set; }

            public string? ProjectLeader { get; set; }

            public string? ProjectFund { get; set; }
        }

        class DataAccess
        {
            /// <summary>
            /// 生产者
            /// </summary>
            public void GetData()
            {
                Task.Run(() =>
                {
                    blockingCollection = new BlockingCollection<ProjectModel>();
                    using (SqlConnection connection = new SqlConnection("Data Source=.;Initial Catalog=cap_demo;Integrated Security=True"))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("select top 500000 Id,PLW_Code as ProjectCode,PLW_Agent as ProjectLeader,PLW_Funds as ProjectFund from Tb_Project", connection);
                        SqlDataReader dr = command.ExecuteReader();//ado.net对像中的只进只读的一个对像，一条一条的读取。这里也可以用其它他方法，例如我一次读1000条或是10000条，都可以。
                        while (dr.Read())
                        {

                            ProjectModel model = new ProjectModel()
                            {

                                Id = dr["Id"]?.ToString(),
                                ProjectCode = dr["ProjectCode"]?.ToString(),
                                ProjectLeader = dr["ProjectLeader"]?.ToString(),
                                ProjectFund = dr["ProjectFund"]?.ToString()
                            };
                            bool res = false;
                            while (res == false)//如果添加失败一直添加，如果地指定的时间添加失败或是集体不允许有重复项
                            {
                                res = blockingCollection.TryAdd(model, 100);
                                if (blockingCollection.Count > 60000)
                                {
                                    Console.WriteLine($"队列已满在:{stopwatch.ElapsedMilliseconds}毫秒,队列有数据{blockingCollection.Count}条");
                                    Thread.Sleep(5000);
                                }
                            }

                            //此课程所有的代码都放到了git上，并且可以免费获取，所以大家一定要练习一下。
                        }
                        blockingCollection.CompleteAdding();
                    }
                });
            }

            int count = 0;
            ISheet sheet1 = null;
            int sheetNum = 0;
            XSSFWorkbook _workbook = null;
            object lockObj = new object();
            /// <summary>
            /// 消费者
            /// </summary>
            public void CreateExcel()
            {
                _workbook = new XSSFWorkbook();
                sheet1 = _workbook.CreateSheet($"Sheet{sheetNum}");

                List<Task> list = new List<Task>();
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));

                Task.WhenAll(list).ContinueWith(t =>
                {
                    stopwatch.Stop();
                    Console.Write("结束导出,耗时:" + stopwatch.ElapsedMilliseconds);

                    using (FileStream file = new FileStream(System.DateTime.Now.Ticks.ToString() + ".xls", FileMode.Create))
                    {
                        _workbook.Write(file);
                    }
                });
            }

            private void Insert()
            {
                while (!blockingCollection!.IsCompleted)

                {

                    bool lockTaken = false;
                    while (lockTaken == false)
                    {
                        Monitor.TryEnter(lockObj, 100, ref lockTaken);
                    }
                    try
                    {
                        if (count > 0 && (count % 100000 == 0))
                        {
                            sheetNum++;
                            count = 0;
                            sheet1 = _workbook.CreateSheet($"Sheet{sheetNum}");
                            Console.WriteLine($"开始创建第{sheetNum}个工作薄");
                        }
                        if (count > 0 && (count % 10000 == 0))
                        {
                            Console.WriteLine($"导入{count}条用时:{stopwatch.ElapsedMilliseconds}毫秒");
                        }
                        ProjectModel? item = null;
                        blockingCollection?.TryTake(out item);
                        if (item != null)
                        {
                            IRow row = sheet1.CreateRow(count);

                            row.CreateCell(0).SetCellValue(item.Id);
                            row.CreateCell(1).SetCellValue(item.ProjectCode);
                            row.CreateCell(2).SetCellValue(item.ProjectLeader);
                            row.CreateCell(3).SetCellValue(item.ProjectFund);
                            count++;

                        }

                    }
                    finally
                    {
                        Monitor.Exit(lockObj);

                    }

                }


            }


        }

    }



}
