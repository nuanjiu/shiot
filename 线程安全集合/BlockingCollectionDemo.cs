﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace 线程安全集合
{
    internal class BlockingCollectionDemo
    {
        internal static int INIT_MAX_COUNT = 10;
        internal static BlockingCollection<int>? BlockingCollection;//在添加或是读取元素之前，会阻塞进程一直等待
        public static void Run(bool fifo=true)
        {
            if (fifo)
            {
                BlockingCollection = new BlockingCollection<int>();//先进先出的队列
            }
            else
            {
                BlockingCollection = new BlockingCollection<int>(new ConcurrentStack<int>());//先进后出的栈
            }
            //只有生产者生产出产品后，消费者才能消费
            Task.Run(() => {
                new Producer().Produce();
            });

            Task.Run(() => {

                new Consumer("产线A").Consume();
            });

            Task.Run(() => {

                new Consumer("产线B").Consume();
            });
            Task.WaitAll();

        }
        /// <summary>
        /// 生产者
        /// </summary>
         class Producer
        {
            public void Produce()
            {
                for (int i = 0; i < INIT_MAX_COUNT; i++)
                {
                    global::System.Console.WriteLine($"A工厂开始生产序号为{i}的产品");
                    BlockingCollection?.TryAdd(i, 1000);//第二个参数是超时时间
                    System.Threading.Thread.Sleep(100);
                }
                BlockingCollection?.CompleteAdding();//完成生成的数量后结束
            }
        }
        class Consumer
        {
            public string _name { get; set; }

            public Consumer(string name)
            {
                _name = name;
            }
            public void Consume()
            {
                while (!BlockingCollection!.IsCompleted)
                {
                    Thread.Sleep(500);
                    bool res = BlockingCollection!.TryTake(out int i, 1000);
                    if(res)
                    {
                        global::System.Console.WriteLine($"{_name}消费了{i}产品");
                    }
                }
            }
        }
        
    }

   
}
