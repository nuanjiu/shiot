﻿using NPOI.HSSF.UserModel;
using NPOI.OpenXmlFormats.Spreadsheet;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace 线程安全集合
{
    public class ExcelOperationV2
    {
        public static BlockingCollection<ProjectModel>? blockingCollection;//在添加或是读取元素之前，会阻塞进程一直等待
        public static ConcurrentBag<BlockingCollection<ProjectModel>> sheetsData = new ConcurrentBag<BlockingCollection<ProjectModel>>();
        public static ConcurrentBag<ISheet> sheets = new ConcurrentBag<ISheet>();
        public static int ThreadNum = 5;//开5个线程
        static int sheetNum = 6;//工作薄数量
        static int perSheetNum = 100000;//每一页显示多少数据
        static int recordNum = 500010;//总数据
        static XSSFWorkbook _workbook = null;
        static Stopwatch stopwatch = new Stopwatch();
        public static void Run()
        {
            Console.Write("开始导出数据");
            stopwatch.Start();
            _workbook = new XSSFWorkbook();//初始化数据
            for (int i = 0; i < sheetNum; i++)
            {
                sheetsData.Add(new BlockingCollection<ProjectModel>());
                sheets.Add(_workbook.CreateSheet($"Sheet{sheetNum}"));
            }

            new DataAccess().GetData();
            new DataAccess().CreateExcel();

        }

        public class ProjectModel
        {
            public string? Id { get; set; }
            public string? ProjectCode { get; set; }

            public string? ProjectLeader { get; set; }

            public string? ProjectFund { get; set; }
        }

        class DataAccess
        {
            /// <summary>
            /// 生产者
            /// </summary>
            public void GetData()
            {

                Task.Run(() =>
                {
                    blockingCollection = new BlockingCollection<ProjectModel>();
                    using (SqlConnection connection = new SqlConnection("Data Source=.;Initial Catalog=cap_demo;Integrated Security=True"))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand($"select top {recordNum} Id,PLW_Code as ProjectCode,PLW_Agent as ProjectLeader,PLW_Funds as ProjectFund from Tb_Project", connection);
                        SqlDataReader dr = command.ExecuteReader();//ado.net对像中的只进只读的一个对像，一条一条的读取。这里也可以用其它他方法，例如我一次读1000条或是10000条，都可以。
                        while (dr.Read())
                        {

                            ProjectModel model = new ProjectModel()
                            {

                                Id = dr["Id"]?.ToString(),
                                ProjectCode = dr["ProjectCode"]?.ToString(),
                                ProjectLeader = dr["ProjectLeader"]?.ToString(),
                                ProjectFund = dr["ProjectFund"]?.ToString()
                            };
                            bool res = false;
                            while (res == false)//如果添加失败一直添加，如果地指定的时间添加失败或是集体不允许有重复项
                            {
                                res = blockingCollection.TryAdd(model, 100);
                                if (blockingCollection.Count > 60000)
                                {
                                    Console.WriteLine($"队列已满在:{stopwatch.ElapsedMilliseconds}毫秒,队列有数据{blockingCollection.Count}条");
                                    Thread.Sleep(5000);
                                }
                            }

                            //此课程所有的代码都放到了git上，并且可以免费获取，所以大家一定要练习一下。
                        }
                        blockingCollection.CompleteAdding();
                    }
                });
            }

            int count = 0;
            ISheet sheet1 = null;


            object lockObj = new object();
            /// <summary>
            /// 消费者
            /// </summary>
            public void CreateExcel()
            {




                List<Task> list = new List<Task>();
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));
                list.Add(Task.Run(() =>
                {
                    Insert();
                }));

                Task.WhenAll(list).ContinueWith(t =>
                {
                    stopwatch.Stop();
                    Console.Write("结束导出,耗时:" + stopwatch.ElapsedMilliseconds);

                    using (FileStream file = new FileStream(System.DateTime.Now.Ticks.ToString() + ".xls", FileMode.Create))
                    {
                        _workbook.Write(file);
                    }
                });
            }

            public void Insert()
            {

            }

        }
    }
}
