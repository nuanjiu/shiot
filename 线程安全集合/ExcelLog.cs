﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using NPOI.SS.Formula.PTG;

namespace 线程安全集合
{
    public class ExcelLog
    {
        SqlConnection sqlConnection;
        public ExcelLog() {
            sqlConnection = new SqlConnection("Data Source=.;Initial Catalog=cap_demo;Integrated Security=True");
        }

        private static readonly string INSEERT_SQL = "insert into tb_data_operation(dataId,dataObjectName,operationTime,operationResult,operationTag)values(@dataId,@dataObjectName,@operationTime,@operationResult,@operationTag)";
        public void Insert(string dataId, string dataObjectName, DateTime operationTime, string operationResult, string operationTag)
        {
            if(sqlConnection.State==System.Data.ConnectionState.Closed)
            {
                sqlConnection.Open();
            }
            SqlParameter[] parameters = { };
            parameters[0].Value = dataId;
            parameters[1].Value = dataObjectName;
            parameters[2].Value = operationTime;
            parameters[3].Value = operationResult;
            parameters[4].Value = operationTag;
        }

    }
}
