﻿using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.SqlServer.Server;
using Project.Domain;

namespace Middleware.Lib
{
    public class ExceptionRequestDelegateMiddleware
    {
        public RequestDelegate? _requestDelegate;
        public ExceptionRequestDelegateMiddleware(RequestDelegate requestDelegate)
        {
            _requestDelegate = requestDelegate;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (_requestDelegate != null)
            {
                try
                {
                    await _requestDelegate.Invoke(context);

                }
                catch (Exception ex)
                {
                    Project.Domain.t_businessprocess t_Businessprocess = new t_businessprocess();
                    using (SqlConnection sqlConnection = new SqlConnection(""))
                    {
                        //sqlConnection.Open();
                    }
                        context.Response.ContentType = "application/json;charset=utf-8";
                    //统一处理这个异常，那么肯定不能直接把异常抛给前台，我们一定要在这里写日志，给前台返回一个友好的错误：如系统发生异常，请重试或联系管理员
                    await context.Response.WriteAsync(ex.Message+"请求路径为，【演示nuget包的使用】:"+context.Request.Path);
                }
            }
        }
    }
}