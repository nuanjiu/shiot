﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Middleware.Lib
{
    public static class MiddlewareExtensions
    {
        /// <summary>
        /// 暴露异常中间件
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseException(this IApplicationBuilder builder)
        {

            return builder.UseMiddleware<ExceptionRequestDelegateMiddleware>();
        }
    }
}
