using Iot.Demo.Middleware;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddSingleton<StopwatchMiddleware>();

var app = builder.Build();
app.UseMiddleware<StopwatchMiddleware>();//基于Imiddleware

app.UseStopwatch();
//app.UseMiddleware<StopwatchMiddleware>();//基于Imiddleware

//app.UseMiddleware<ExceptionRequestDelegateMiddleware>();//基于约束



app.UseException();



//终结
//app.Run(async context =>
//{
//    var c = context;
//    await context.Response.WriteAsync("hello world");
//});
//可以通过不调用next.Invoke实现短路
//app.Use(async (context, next) =>
//{
//    var path = context.Request.Path;
//    Console.WriteLine("user_begin");
//    Console.WriteLine(path);
//    await next.Invoke();
//    Console.WriteLine("user_end");


//});
//路由为/api/demo开头的请求方法才会执行
//app.Map("/api/demo", app =>
//{

//    app.Use(async (context, next) =>
//    {
//        var path = context.Request.Path;
//        Console.WriteLine("api/demo_user_begin");
//        Console.WriteLine(path);
//        await next.Invoke();
//        Console.WriteLine("api/demo_user_end");
//    });
//});
//app.UseWhen(context => true, app =>
//{
//    app.Use(async (context, next) =>
//    {
//        var path = context.Request.Path;
//        Console.WriteLine("userwhen_begin");
//        Console.WriteLine(path);
//        await next.Invoke();
//        Console.WriteLine("userwhen_end");

//    });

//});
//app.MapWhen(context => true, app =>
//{
//    app.Use(async (context, next) =>
//    {
//        var path = context.Request.Path;
//        Console.WriteLine("mapwhen_begin");
//        Console.WriteLine(path);
//        await next.Invoke();

//        Console.WriteLine("mapwhen_end");

//    });

//});

//这里模拟是当请求路径包括api/demo的时候才会执行，useWhen如果符合条件不会替换主管道
//app.UseWhen(context => context.Request.Path.ToString().ToLower().Contains("api/demo"), app =>
//{
//    app.Use(async (context, next) =>
//    {
//        var path = context.Request.Path;
//        Console.WriteLine("userwhen_begin");
//        Console.WriteLine(path);
//        await next.Invoke();
//        Console.WriteLine("userwhen_end");

//    });

//});

//这里模拟当请求方式为get时，如果符合条件，mapwhen会替换主管道。
//app.MapWhen(context => context.Request.Method.ToLower() == "get", app =>
//{
//    app.Use(async (context, next) =>
//    {
//        var path = context.Request.Path;
//        Console.WriteLine("mapwhen_begin");
//        Console.WriteLine(path);
//        await next.Invoke();

//        Console.WriteLine("mapwhen_end");

//    });

//});

app.UseRouting();
app.UseAuthorization();
app.MapControllers();



app.Run();
