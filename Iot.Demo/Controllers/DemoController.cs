﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Iot.Demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "ok";
        }

        [HttpGet]
        [Route("/test/api/demo")]
        public string Get1()
        {
            Thread.Sleep(10000);//模拟接口请求响应时间过长。
            return "ok";
        }

        [HttpGet]
        [Route("/test/api/error")]
        public string GetError()
        {
            throw new Exception("模拟发生异常");
        }
    }
}
