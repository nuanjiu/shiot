﻿using System;
using System.Diagnostics;

namespace Iot.Demo.Middleware
{
    public class StopwatchMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            Stopwatch stopwatch = new Stopwatch();//定义一个性能监控类
            stopwatch.Start();
            await next.Invoke(context);
            stopwatch.Stop();
            Console.WriteLine($"请求接口的路径是{context.Request.Path},方法执行间隔为{stopwatch.ElapsedMilliseconds}毫秒");
        }
    }
}
