﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Iot.Demo.Middleware
{
    public static class MiddlewareExtensions
    {
        /// <summary>
        /// 暴露性能监控中间件
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseStopwatch(this IApplicationBuilder builder)
        {

            return builder.UseMiddleware<StopwatchMiddleware>();
        }
        /// <summary>
        /// 暴露异常中间件
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseException(this IApplicationBuilder builder)
        {

            return builder.UseMiddleware<ExceptionRequestDelegateMiddleware>();
        }
    }
}
