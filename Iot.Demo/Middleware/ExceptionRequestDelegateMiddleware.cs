﻿using static System.Net.Mime.MediaTypeNames;
using System.Diagnostics;

namespace Iot.Demo.Middleware
{
    public class ExceptionRequestDelegateMiddleware
    {
        public RequestDelegate? _requestDelegate;
        public ExceptionRequestDelegateMiddleware(RequestDelegate requestDelegate)
        {
            _requestDelegate = requestDelegate;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (_requestDelegate != null)
            {
                try
                {
                    await _requestDelegate.Invoke(context);
                  
                }
                catch(Exception ex)
                {
                    context.Response.ContentType = "application/json;charset=utf-8";
                    await context.Response.WriteAsync(ex.Message);
                }
            }
        }
    }
}
