﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapperDemo.dto;
using AutoMapperDemo.models;
using Net6.DbContext.Models;

namespace AutoMapperDemo
{
    public class AutoMapperProfiler:Profile
    {
        public AutoMapperProfiler()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<TeacherListDto, tb_teacher>();
            CreateMap<tb_teacher, TeacherListDto>();
            CreateMap<TeacherAddDto, tb_teacher>();
            CreateMap<tb_teacher, TeacherAddDto>();
        }
    }
}
