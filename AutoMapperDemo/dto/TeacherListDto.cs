﻿using System.ComponentModel.DataAnnotations;

namespace AutoMapperDemo.dto
{
    public class TeacherListDto
    {
        public int Id { get; set; } 
        [Required]
        [MaxLength(10)]
        public string TeachName { get; set; }
        /*这里只是强调大家要做好Dto验证*/

        public DateTime CreateTime { get; set; }

    }
}
