﻿using System.ComponentModel.DataAnnotations;

namespace AutoMapperDemo.dto
{
    public class TeacherAddDto
    {
        [Required]
        [MaxLength(10)]
        public string TeachName { get; set; }
        /*这里只是强调大家要做好Dto验证*/

    }
}
