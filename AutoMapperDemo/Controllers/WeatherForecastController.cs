using AutoMapper;
using AutoMapperDemo.dto;
using AutoMapperDemo.models;
using Microsoft.AspNetCore.Mvc;
using Net6.DbContext;
using Net6.DbContext.Models;

namespace AutoMapperDemo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };
        private readonly IMapper _mapper;

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly Net6DbContext _net6DbContext;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IMapper mapper, Net6DbContext net6DbContext)
        {
            _logger = logger;
            _mapper = mapper;
            _net6DbContext = net6DbContext;
        }

        [HttpGet]
        public UserDto Get()
        {
            User user = new User() { Id = 1, Name = "张三", Email = "aaaaa@qq.com" };
            UserDto dto = _mapper.Map<UserDto>(user);

            List<User> list = new List<User>();
            list.Add(user);

            List<UserDto> lisDto = _mapper.Map<List<UserDto>>(list);

            return dto;
        }

        /// <summary>
        /// 添加教师
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public bool AddTeacher(TeacherAddDto m)
        {
            tb_teacher entity = new tb_teacher();
            entity.TeachName = m.TeachName;
            entity.CreateTime = System.DateTime.Now;
            _net6DbContext.tb_Teachers.Add(entity);
            _net6DbContext.SaveChanges();
            return true;
            /*这里是demo代码，给大家看看怎么样实现dto到实体的转换*/
        }
        /// <summary>
        /// 添加教师
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public bool AddTeacherAutoMapper(TeacherAddDto m)
        {
            tb_teacher entity = new tb_teacher();
            entity = _mapper.Map<tb_teacher>(m);
            _net6DbContext.tb_Teachers.Add(entity);
            _net6DbContext.SaveChanges();
            return true;
            /*这里是demo代码，给大家看看怎么样实现dto到实体的转换*/
        }

        public List<TeacherListDto> GetList()
        {
            List<tb_teacher> data = _net6DbContext.tb_Teachers.ToList();//可以看看前面讲的那个视频里避免过早ToList问题

            List<TeacherListDto> res = new List<TeacherListDto>();
            data.ForEach(x =>
            {
                res.Add(new TeacherListDto() { Id = x.Id, CreateTime = x.CreateTime, TeachName = x.TeachName });
            });

            return res;
        }

       

        public List<TeacherListDto> GetListAutoMapper()
        {
            List<tb_teacher> data = _net6DbContext.tb_Teachers.ToList();//可以看看前面讲的那个视频里避免过早ToList问题

            List<TeacherListDto> res = _mapper.Map<List<TeacherListDto>>(data);

            return res;
        }

    }
}